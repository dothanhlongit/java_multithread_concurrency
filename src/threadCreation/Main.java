package threadCreation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String [] args) {
		List<Runnable> threads = new ArrayList<>();
		threads.add(new CustomThread());
		threads.add(new CustomThread());
		threads.add(new CustomThread());
		threads.add(new CustomThread());

		MultiExecutor multiExecutor = new MultiExecutor(threads);
		multiExecutor.executeAll();

	}
	public static class CustomThread extends Thread {
		@Override
		public void run() {
			System.out.println(Thread.currentThread().getName() + " is running...");
			super.run();
		}
	}
}
