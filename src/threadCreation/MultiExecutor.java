package threadCreation;

import java.util.List;

public class MultiExecutor {

	private List<Runnable> threads;

	public MultiExecutor(List<Runnable> tasks) {
		threads = tasks;
	}

	/**
	 * Starts and executes all the tasks concurrently
	 */
	public void executeAll() {
		for (Runnable thread : threads) {
			thread.run();
		}
	}
}