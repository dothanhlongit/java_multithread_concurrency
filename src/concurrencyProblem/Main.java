package concurrencyProblem;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Main {
	private static final int SLEEP_TIME = 5000;

	public static void main(String[] args) throws InterruptedException {
		MinMaxMetrics minMaxMetrics = new MinMaxMetrics();
		Thread thread1 = new Thread(() -> {
			while (true) {
				minMaxMetrics.addSample(new Random().nextInt());
				try {
					TimeUnit.MILLISECONDS.sleep(SLEEP_TIME);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		Thread thread2 = new Thread(() -> {
			while (true) {
				minMaxMetrics.addSample(new Random().nextInt());
				try {
					TimeUnit.MILLISECONDS.sleep(SLEEP_TIME);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		thread1.start();
		thread2.start();

		while (true) {
			System.out.println("min: " + minMaxMetrics.getMin());
			System.out.println("max: " + minMaxMetrics.getMax());
			TimeUnit.MILLISECONDS.sleep(SLEEP_TIME - 1000);
		}
	}

	public static class MinMaxMetrics {
		// Add all necessary member variables
		private volatile long min;
		private volatile long max;

		/**
		 * Initializes all member variables
		 */
		public MinMaxMetrics() {
		}

		/**
		 * Adds a new sample to our metrics.
		 */
		public synchronized void addSample(long newSample) {
			System.out.println("current thread: " +
					Thread.currentThread().getName() + ", add sample: " + newSample);
			if (newSample > max) {
				max = newSample;
			}
			if (newSample < min) {
				min = newSample;
			}
		}

		/**
		 * Returns the smallest sample we've seen so far.
		 */
		public long getMin() {
			return this.min;
		}

		/**
		 * Returns the biggest sample we've seen so far.
		 */
		public long getMax() {
			return this.max;
		}
	}
}