package threadCordination;

import java.math.BigInteger;

public class Main {
	public static void main(String[] args) throws InterruptedException {
		BigInteger result = new ComplexCalculation().calculateResult(new BigInteger("5"), new BigInteger("10"),
				new BigInteger("15"), new BigInteger("20"));

		System.out.println(result);
	}
}